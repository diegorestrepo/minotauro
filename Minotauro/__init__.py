from .photovoltaic_panel import PhotovoltaicPanel
from .wind_turbine import WindTurbine
from .golden_section import GoldenSection
from .simulated_annealing import SimulatedAnnealing
from .partial_shading_scenarios import parcial0, parcial1, parcial2, parcial3
