'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''


class GoldenSection:
    '''Search Golden Section.'''

    def __init__(self, ax, bx, f):
        self.ax = ax
        self.bx = bx
        self.f = f

    def search(self):
        R = 0.61803399
        C = 1 - R

        control = 0

        xl = self.ax
        xu = self.bx

        while True:
            x1 = xl + R*(xu - xl)
            x2 = xl + C*(xu - xl)

            f1 = self.f(x1)
            f2 = self.f(x2)

            if f2 < f1:
                xl = x2
                xu = xu
                xop = x1
                fop = f1
            else:
                xl = xl
                xu = x1
                xop = x2
                fop = f2

            control = xop/xu

            if control > 0.99:
                break

        return xop, fop
