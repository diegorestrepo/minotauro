'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''


import random
import numpy as np


class SimulatedAnnealing:
    '''Search Simulate Annealing.'''

    def __init__(self, T, Tmin, tol, alfa, f, rank):
        self.T = T
        self.Tmin = Tmin
        self.tol = tol
        self.alfa = alfa
        self.f = f
        self.rank = rank
        self.Vi = 0.0
        self.Vk = 0.0
        self.Pi = 0.0
        self.Pk = 0.0
        self.dP = 0.0
        self.Pe = 0.0

    def equalize(self):
        self.Vi, self.Pi = self.Vk, self.Pk

    def boltzmann(self):
        self.dP = -(self.Pk - self.Pi)
        self.Pe = np.exp(self.dP/self.T)

    def search(self):
        control = 1

        self.Vk = 0.001
        self.Vi = random.uniform(0, 1)
        self.Pi = self.f(self.Vi)
        self.Vk = self.Vi*self.Vk

        count = 0
        fcount = 4

        while(control == 1):
            random.seed(self.Vk)
            self.Vk = random.random()
            self.Vk = self.Vk*self.rank
            self.Pk = self.f(self.Vk)

            if self.Pk > self.Pi:
                self.equalize()
            else:
                self.boltzmann()

                if self.Pe <= self.tol:
                    self.equalize()
                else:
                    self.Vi, self.Pi = self.Vi, self.Pi

            if count < fcount:
                count += 1
            else:
                self.T = self.T - self.alfa*self.T
                count = 0

                if self.T > self.Tmin:
                    control = 1
                else:
                    control = 0

        return self.Vi, self.Pi
