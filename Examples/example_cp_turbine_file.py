from Minotauro import WindTurbine as Turbine


def run():
    turbine = Turbine(cp=0.1,
                      rho=1.225,
                      area=1.0751,
                      velwind=13.0,
                      c1=0.5176,
                      c2=116,
                      c3=0.4,
                      c4=5,
                      c5=21,
                      c6=0.0068,
                      lambda0=0.0,
                      beta=0.0,
                      lambdai=1.0)

    step = 20

    lambda0, cp = turbine.find_cp(step)

    with open('output_cp_turbine.dat', 'w') as f:
        for i in range(step):
            f.write('{valorLambda0:>10.6f} {valorCp:>10.6f}\n'.format(
                valorLambda0=lambda0[i], valorCp=cp[i]))


if __name__ == '__main__':
    run()
