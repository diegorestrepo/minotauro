# Minotauro Project

Modeling of all the elements that make up a hybrid solar-wind system, integration of its components and 
some control algorithms.

## Install packages:
`python3 -m pip install -r requirements.txt`
